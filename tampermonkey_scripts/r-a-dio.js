// ==UserScript==
// @name         SS-R/a/dio
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Send data to song stats server when listening to r-a-d.io
// @author       Cosmin Clucerescu
// @match        http://r-a-d.io/
// @grant GM_xmlhttpRequest
// ==/UserScript==

(function() {
    'use strict';
    var nowPlaying = $('#np');
    var prev_artist = nowPlaying[0].textContent.split('-');
    var prev_title= prev_artist[1];
    prev_artist=prev_artist[0];
    prev_artist.trim();
    prev_title.trim();
    $('#np').on('DOMNodeInserted', function(){
    var nowPlaying = $('#np');
    var artist = nowPlaying[0].textContent.split('-');
    var title= artist[1];
    artist=artist[0];
    artist.trim();
    title.trim();
    if (title != prev_title || artist !=prev_artist)
    {GM_xmlhttpRequest({
        method: "POST",
        // TODO: This should probably be asked but whatever.
        url: "<Your sever here>",
        data: JSON.stringify({"artist": artist,
               "title": title
              }),
        onerror: function(errMsg) {
        alert(errMsg);
        },
        headers:    {
        "Content-Type": "application/json; charset=utf-8"
    },
        responseType: "json"
    });
     prev_artist=artist;
     prev_title=title;
    }
    });
})();
from __future__ import print_function, unicode_literals
import json

import os
from threading import Thread
from time import sleep

import requests
from pynput import keyboard

if "SS_URL" not in os.environ:
    raise Exception("SET SS_URL env var to the url address of your stats server")
SS_URL = os.environ["SS_URL"].rstrip("/")

RATE_LAST_URL = SS_URL + "/rate_last_played"
GET_LAST_RATED_URL = SS_URL + "/last_rated"
GET_LAST_PLAYED_URL = SS_URL + "/last_played"


class WorkerThread(Thread):
    def __init__(self, pressed_keys):
        super(WorkerThread, self).__init__()
        self.last_rated = {'title': None, "artist": None}
        self.pressed_keys = pressed_keys

    def run(self):
        while True:
            try:
                if not self.pressed_keys:
                    continue
                last_rated = self.last_rated
                last_played = get_last_played()
                while last_rated['title'] == last_played['title'] and last_rated['artist'] == last_played['artist']:
                    sleep(5)
                    last_played = get_last_played()
                rating = int(self.pressed_keys[-1])
                if not rating:
                    rating = 10
                response = requests.post(RATE_LAST_URL, json={"rating": rating})
                if response.status_code == 201:
                    data = json.loads(response.text)
                    self.last_rated = data
                    print("Rated {} - {} with {}".format(data['artist'], data['title'], rating))
                else:
                    print("Failed to rate last errcode:{}, content: {}".format(response.status_code, response.content))
            except Exception as error:
                print("Failed with unhandled exception {}".format(error))
                pass
            self.pressed_keys = []


class KeyShortcutHandler(object):

    def __init__(self, shortcuts):
        self.pressed_keys = list()
        self.shortcuts = shortcuts
        self.cancel = False
        self.last_rated = {}

    def on_press(self, key):
        key = getattr(key, "name", getattr(key, "char", ""))
        if not key:
            return
        key = key.split('_')[0]
        if key not in self.pressed_keys:
            self.pressed_keys.append(key)
            shortcut = " ".join(self.pressed_keys)
            callback = self.shortcuts.get(shortcut)
            if callback:
                callback(list(self.pressed_keys))

    def on_release(self, key):
        key = str(key).strip("'")
        try:
            if key == self.pressed_keys[-1]:
                self.pressed_keys.pop(-1)
            else:
                # If release is out of order it's safe to assume that no keyboard shortcut follows.
                self.pressed_keys = []
        except IndexError:
            pass

    def __call__(self):
        # Collect events until released
        with keyboard.Listener(on_press=self.on_press, on_release=self.on_release) as listener:
            listener.join()


def get_last_rated():
    response = requests.get(GET_LAST_RATED_URL)
    return json.loads(response.text)


def get_last_played():
    response = requests.get(GET_LAST_PLAYED_URL)
    return json.loads(response.text)


worker_thread = WorkerThread([])
worker_thread.start()


def rate_callback(pressed_keys):
    worker_thread.pressed_keys = pressed_keys


def get_rating_dict():
    """

    :return:
    """
    ret_val = {}
    for i in range(0, 10):
        ret_val["ctrl alt {}".format(i)] = rate_callback
    return ret_val


def main():
    rating_dict = get_rating_dict()
    KeyShortcutHandler(rating_dict)()


if __name__ == "__main__":
    main()

"""
Monitor clementine for the current song and send data to an ss server.
This could be also implemented in a clementine fork.
"""
from __future__ import unicode_literals
from __future__ import print_function
import time

import os
import re
import requests
from memorpy import MemWorker
from HTMLParser import HTMLParser

if "SS_URL" not in os.environ:
    raise Exception("SET SS_URL env var to the url address of your stats server")
SS_URL = os.environ["SS_URL"].rstrip("/")

INCREASE_PLAY_COUNT_URL = SS_URL + "/increase_play_count"


def increase_play_count(artist, title):
    """

    :param artist:
    :param title
    :return:
    """
    if not artist or not title:
        return
    response = requests.post(INCREASE_PLAY_COUNT_URL, json={"artist": artist, "title": title})
    if response.status_code == 201:
        print("Increased play count for {} {}".format(artist, title))
    else:
        print("Failed to increase play count for {} {} errcode:{}, content: {}".format(artist, title,
                                                                                       response.status_code,
                                                                                       response.content))


class ClementineWatcher(object):
    def __init__(self):
        self.memory_worker = MemWorker(name="clementine")
        self.artist = self.title = None
        self._past_data = {

        }  # address : data mapping
        # Clementine does take awhile to garbage collect, I shall assume that when a garbage collection happens a new
        # song is added (bad assumption).
        self._get_count = 0  # Might as well rescan the memory every now and then.

    def _read_from_address(self, address):
        """
        Attempt to get some relevant data from the address.
        :return:
        """
        a_data = address.read(1500)
        start = 0
        while start < 250:
            potenial_data = a_data[start:].decode("utf-16-le", errors='replace')
            if "Artist" in potenial_data and "Title" in potenial_data:
                return potenial_data
            start += 1

    def _search_memory(self):
        """
        Search the memory for addresses that might contain the currently playing Artist/Title.
        :return:
        """
        address_list = [x for x in self.memory_worker.umem_search("<p align=\"right\">Artist")]
        ret_data = {}
        for address in address_list:
            address -= 500
            data = self._read_from_address(address)
            if data:
                data = self.get_artist_title(data)
                ret_data[str(address)] = (data, address)
        return ret_data

    def _get_data(self):
        """
        Get artist/title data for the currently playing song.
        :return:
        """
        new_data = {}
        search = False
        for str_addr, past_data in self._past_data.items():
            data = self._read_from_address(past_data[1])
            if data:
                data = self.get_artist_title(data)
                if data != past_data[0]:
                    new_data[str_addr] = (data, past_data[1])
            elif not data:
                search = True
                self._past_data.pop(str_addr)

        self._get_count += 1
        if search or self._get_count > 20 or not self._past_data:
            self._get_count = 0
            new_data.update(self._search_memory())
        ret_val = []
        for str_addr, data_addr in new_data.items():
            addr = data_addr[1]
            if str_addr in self._past_data:
                if data_addr[0] != self._past_data[str_addr][0]:
                    ret_val.append(data_addr[0])
            else:
                ret_val.append(data_addr[0])
            self._past_data[str_addr] = (data_addr[0], addr)
        return ret_val

    def get_artist_title(self, data):
        """
        Try to get artist and title from the given data.
        expected format is:
        <p align="right">Title:</p>
              </td>
              <td>No Light</td>
            </tr>
            <tr>
              <td>
                <p align="right">Artist:</p>
              </td>
              <td>emptyself</td>
            </tr>
        :param data:
        :return:
        """
        data = HTMLParser().unescape(data)
        title = artist = None
        title_follows = artist_follows = False
        for line in data.split("\n"):
            if "Artist:" in line:
                artist_follows = True
            if "Title:" in line:
                title_follows = True
            match = re.search(r"<td>(.*)<\/td>", line)
            if artist_follows and match:
                artist = match.group(1)
                if artist and title:
                    return artist, title
                artist_follows = False
            if title_follows and match:
                title = match.group(1)
                if artist and title:
                    return artist, title
                title_follows = False
        return None, None

    def watch(self):
        """

        :return:
        """
        while True:
            data = self._get_data()
            for elem in data:
                artist, title = elem
                if artist != self.artist or title != self.title:
                    increase_play_count(artist, title)
                    self.artist = artist
                    self.title = title
            time.sleep(1)


if __name__ == "__main__":
    ClementineWatcher().watch()
